import sys
import requests
from SafeRequest.SafeRequest import safe_request


def download_image(url: str, file_name: str):
    try:
        img = requests.get(url, timeout=5)
        img.raise_for_status()
    except requests.exceptions.Timeout:
        print("El server tardó demasiado en responder")
        return
    
    with open(file_name, "wb") as handler:
        handler.write(img.content)


def main(*args):
    URL = "https://gelbooru.com/index.php?page=dapi&s=post&q=index"

    DATA = {
        "limit": 1,
        "tags": "cirno rating:safe sort:random -gore -cosplay",
        "json": 1,
        }
    
    resp = safe_request(URL, req_type = "GET", Data = DATA)

    if resp is None:
        return
    
    img_dict = resp["post"][0]

    print("Downloading Cirno image")

    download_image(img_dict["file_url"], img_dict["image"])


if __name__ == "__main__":
    main(sys.argv[1:])


